# Hoo-Wee it's GUI, but are we doing it right?
1) #### What is the user experience?  
When making a GUI interface, the first thing that you should think about is "Who is my audience". The reason for this consideration is because your audience is going to differ on the speed, the hardware, the time, and a bunch of other factors in how they use your application.  
* For example - An elderly person may be unwilling to use touch aspects of an application, whereas a younger person may be fine with it.

2) #### Keep it simple and consistent
Make sure that the information on your GUI application is constantly. Users like to know where their stuff is, and they like to learn where it is quickly. It is for that reason that it's important to make sure that frequently used information is highlighted in it's level of interest, and that it remains consistent in its placement when going to other parts of the GUI application.

3) #### Create a visual hierarchy
Simple, the most used and most important information should be what's eye catching and easily accessible. The user shouldn't have to work hard to get what they want out of a GUI application, so GUI applications should be designed to prevent that difficulty.

4) #### Use good typography
People like to read when they learn. Make sure that the text you use is not only appropriate for the theme of the designed GUI application, but also easily readable. Considering principles like letter-spacing, font-kerning, that effect the shape of the text displayed.

5) #### Use colour and contrast properly
Your background should be just that, your background. For almost any colour, you'll find a contrasting value that you can use to highlight information. Black has white, orange has purple. Using contrast not only improves the quality of your GUI application, but the engagement a user will have with that application.

6) #### Consider a feedback method
Keep their attention while something is happening. Consider what a user might think while something is loading, can  they be more engaged. Often the answer is yes. Try adding a loading bar that quotes something relating to your applciation.

7) #### Merge similar functions
Make sure that like is with like. If a user is looking for something, they're going to be looking for what they consider to be a similar feature. Merging similar functions increases the speed someone can navigate a GUI application.
* In our example - This would be combining the information about the reds together, the greens together, and the blues together.

8) #### Make helpful recommendations
The last point that I'll make is about helping the user out. Consider this, who knows more about the application, you or the user of your GUI application? Almost always, this will be you, the creator. As such, you should consider helping the user of your application whenever possible.
* Guide them with an optional, built in tutorial
* Direct them to helpful videos that you, or someone else, may have made
* Store data about them and make recommendations that you feel will be useful to that user type.

---

#### References:
https://www.elegantthemes.com/blog/resources/10-rules-of-good-ui-design-to-follow-on-every-web-design-project

http://vanseodesign.com/web-design/color-meaning/

http://www.goodui.org/

---

#### Peer Review

I really like that James has expressed just how important it is to keep the user of the application in mind, when designing a GUI application. I feel that he has thoroughly discussed many of the principles of GUI design, and explained each of them in terms of how they positively impact on an application's usability. As someone that has never really done much GUI design, James' report has definetely opened my eyes about the different GUI design practices, and I will definetely be implementing these principles in my own applications in the future.

*Rhiannon Rogers*

